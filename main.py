import os
import random
import time
import blas_module






def test_vector():
    d = {}
    for f in range(10000, 100000, 10000):
        firstVector = []
        secondVector = []
        for i in range(0, f):
             ele = random.uniform(1.5, 1.9)
             firstVector.append(ele)
        for i in range(0, f):
             ele = random.uniform(1.5, 1.9)
             secondVector.append(ele)
        const = 5
        d[f] = {}
        temp = time.time()
        blas_module.calculate_vector(firstVector, secondVector, const)
        d[f]['end_time'] = time.time() - temp
    return d
def experiment():
    result = test_vector()
    for a in result:
        print(str(a) + ' ' + str(result[a]['end_time']))
experiment()
